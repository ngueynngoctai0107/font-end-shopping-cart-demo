import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RequestUser } from '../../../models/requestUser';
import { AuthService } from '../../../services/auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	username: string = '';
	password: string = '';
	signedinUser: string;

	public isError: boolean = false;

	constructor(private authService: AuthService, private router: Router) { }

	ngOnInit(): void {
		if (this.authService.isUserLoggedIn()) {
			if (!this.authService.getRole()) {
				this.router.navigateByUrl('userlayout');
			}
			else {
				this.router.navigateByUrl('adminlayout');
			}

		}
	}

	doSignin() {
		if (this.username !== '' && this.username !== null && this.password !== '' && this.password !== null) {
			const request: RequestUser = {
				username: this.username, password: this.password,
			};
			this.authService.signin(request).subscribe((result) => {
				this.signedinUser = result.roles[0];
				if (this.signedinUser == "ADMIN") {
					this.router.navigateByUrl('adminlayout');
				}
				else {
					this.router.navigateByUrl('userlayout');
				}
			}, () => {
				this.showAlert();
			});
		} else {
			this.showAlert();
		}
	}

	showAlert(): void {
		if (this.isError) {
			return;
		}
		this.isError = true;
		setTimeout(() => this.isError = false, 2500)
	}

}
