import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RequestUser } from '../../../models/requestUser';
import { AuthService } from '../../../services/auth.service';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent {
	roles: any = ['ADMIN', 'USER', 'SUPPER_ADMIN'];
	username: string = '';
	password: string = '';
	email: string = '';
	fullName: string = '';
	selectedRoles: string;

	public isError: boolean = false;

	constructor(private authService: AuthService, private router: Router) { }

	doSignup() {
		if (this.username !== '' && this.username !== null && this.password !== '' && this.password !== null && this.selectedRoles !== null) {
			const request: RequestUser = { username: this.username, email: this.email, password: this.password, fullName: this.fullName, roles: this.selectedRoles };
			this.authService.signup(request).subscribe((result) => {
				this.router.navigateByUrl('login');
			}, (err) => {

			});
		} else {
			this.showAlert();
		}
	}

	onChangeRole(event: Event) {
		this.selectedRoles = (event.target as HTMLInputElement).value
	}

	showAlert(): void {
		if (this.isError) {
			return;
		}
		this.isError = true;
		setTimeout(() => this.isError = false, 2500)
	}

}
