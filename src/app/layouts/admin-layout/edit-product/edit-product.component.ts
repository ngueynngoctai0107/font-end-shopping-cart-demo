import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/models/category';
import { Product } from 'src/app/models/product';
import { AuthService } from 'src/app/services/auth.service';
import { CategoryService } from 'src/app/services/category.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  form: FormGroup;
  category: Category;
  categories: Category[] = [];
  category_id: any;
  category_name: string;
  id: number;
  product: Product;
  imageSrc: any;

  public isError: boolean = false;
  public isSuccess: boolean = false;

  checkUploadImg: boolean = false;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private categoryService: CategoryService,
    public fb: FormBuilder,
    public productService: ProductService,
    public router: Router
  ) {
    this.form = this.fb.group({
      name: [''],
      price: [''],
      totals: [''],
      image: [null]
    })
  }

  ngOnInit(): void {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    if (!this.authService.getRole()) {
      this.router.navigateByUrl('login');
    }
    this.id = this.route.snapshot.params['id'];
    this.productService.getProductById(this.id)
      .subscribe(data => {
        console.log(data);
        this.category = data.category;
        this.category_id = this.category.id;
        this.product = data;
        this.form = this.fb.group({
          name: [this.product.name],
          price: [this.product.price],
          totals: [this.product.totals],
          image: [null]
        })

        this.imageSrc = 'data:image/jpeg;base64,' + this.product.picByte;
      }, error => console.log(error));

    this.categoryService.getAll().subscribe(
      data => {
        for (let value of data) {
          if (value.id !== this.product.category.id) {
            this.categories.push(value);
          }
        }
      }, error => console.log(error));
  }

  onChangeCategory(event: Event) {
    this.category_id = (event.target as HTMLInputElement).value
  }

  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({
      image: file
    });
    this.form.get('image').updateValueAndValidity()

    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
      };
    }

    this.checkUploadImg = true;
  }

  submit() {
    if (this.checkUploadImg) {
      this.productService.updateProduct(
        this.id,
        this.form.value.name,
        this.form.value.price,
        this.form.value.totals,
        this.category_id,
        this.form.value.image,
      ).subscribe(
        data => {
          console.log(data);
          if (this.isSuccess) {
            return;
          }
          this.isSuccess = true;
          setTimeout(() => this.isSuccess = false, 2500)
        }, (err) => {
          if (this.isError) {
            return;
          }
          this.isError = true;
          setTimeout(() => this.isError = false, 2500)
        }
      );
    }
    else {
      this.productService.updateProductNoImg(
        this.id,
        this.form.value.name,
        this.form.value.price,
        this.form.value.totals,
        this.category_id,
      ).subscribe(
        data => {
          console.log(data);
          if (this.isSuccess) {
            return;
          }
          this.isSuccess = true;
          setTimeout(() => this.isSuccess = false, 2500)
        }, (err) => {
          if (this.isError) {
            return;
          }
          this.isError = true;
          setTimeout(() => this.isError = false, 2500)
        }
      );
    }
  }

}
