import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from '../../../models/category';
import { AuthService } from '../../../services/auth.service';
import { CategoryService } from '../../../services/category.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent implements OnInit {
  form: FormGroup;
  id: number;
 
  public isError: boolean = false;
  public isSuccess: boolean = false;

  constructor(private categoryService: CategoryService, private route: ActivatedRoute, private router: Router,
    public fb: FormBuilder,
    private authService: AuthService) {
    this.form = this.fb.group({
      name: [''],
      title: [''],
    })
  }

  ngOnInit(): void {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    if (!this.authService.getRole()) {
      this.router.navigateByUrl('login');
    }
    this.id = this.route.snapshot.params['id'];
    this.categoryService.findById(this.id)
      .subscribe(data => {
        console.log(data)
        this.form = this.fb.group({
          name: [data.name],
          title: [data.title],
        })
      }, error => console.log(error));
  }

  submit() {
    const category: Category = { id: this.id, name: this.form.value.name, title: this.form.value.title };
    this.categoryService.updateCategory(category).subscribe(data => {
      console.log(data)
      if (this.isSuccess) {
        return;
      }
      this.isSuccess = true;
      setTimeout(() => this.isSuccess = false, 2500)
    }, (err) => {
      if (this.isError) {
        return;
      }
      this.isError = true;
      setTimeout(() => this.isError = false, 2500)
    });
  }
}
