import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../../models/product';
import { AuthService } from '../../../services/auth.service';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  products: Product[];
  base64Data: any;
  namefind: string = '';

  public isError: boolean = false;
  public isSuccess: boolean = false;

  constructor(private productService: ProductService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    if (!this.authService.getRole()) {
      this.router.navigateByUrl('login');
    }
    this.getAllProduct();
  }

  getAllProduct() {
    this.productService.getAllProduct().subscribe(
      data => {
        this.products = data;
      },
      error => console.log("khong co quyen"),
    );
  }

  handleImage(picByte) {
    this.base64Data = picByte;
    return 'data:image/jpeg;base64,' + this.base64Data;
  }

  findname(namefind: string){
    this.productService.findProduct(namefind).subscribe(data => {
      this.products = data
    });
  }

  onEditP(id: number) {
    this.router.navigate(['/adminlayout/update-product/', id]);
  }

  onDeleteP(id: number) {
    this.productService.deleteProduct(id).subscribe(
      data => {
        console.log(data);
        if (this.isSuccess) {
          return;
        }
        this.isSuccess = true;
        setTimeout(() => this.isSuccess = false, 2500)
        this.getAllProduct();
      },
      error => {
        if (this.isError) {
          return;
        }
        this.isError = true;
        setTimeout(() => this.isError = false, 2500)
      }
    );
  }

}
