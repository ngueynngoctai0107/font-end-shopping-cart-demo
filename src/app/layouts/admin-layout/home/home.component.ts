import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from '../../../models/category';
import { Product } from '../../../models/product';
import { AuthService } from '../../../services/auth.service';
import { CategoryService } from '../../../services/category.service';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  categories: Category[];
  products: Product[];
  base64Data: any;

  constructor(private categoryService: CategoryService,
    private productService: ProductService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    if (!this.authService.getRole()) {
      this.router.navigateByUrl('login');
    }
    this.getAllProduct();
    this.getAllCategory(); 
  }

  getAllCategory() {
    this.categoryService.getAll().subscribe(
      data => {
        console.log(data);
        this.categories = data;
      },
      error => console.log("khong co quyen"),
    );
  }

  getAllProduct() {
    this.productService.getAllProduct().subscribe(
      data => {
        console.log(data);
        this.products = data;
      },
      error => console.log("khong co quyen"),
    );
  }

  handleImage(picByte) {
    this.base64Data = picByte;
    return 'data:image/jpeg;base64,' + this.base64Data;
  }

}
