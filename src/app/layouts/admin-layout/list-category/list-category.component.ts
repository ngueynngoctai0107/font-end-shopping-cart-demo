import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from '../../../models/category';
import { AuthService } from '../../../services/auth.service';
import { CategoryService } from '../../../services/category.service';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.css']
})
export class ListCategoryComponent implements OnInit {
  categories: Category[];
  base64Data: any;
  success: boolean = false;

  public isError: boolean = false;
  public isSuccess: boolean = false;

  constructor(private categoryService: CategoryService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    if (!this.authService.getRole()) {
      this.router.navigateByUrl('login');
    }
    this.getAllCategory();
  }

  getAllCategory() {
    this.categoryService.getAll().subscribe(
      data => {
        this.categories = data;
        console.log(data);
      },
      error => console.log("khong co quyen"),
    );
  }

  onEditC(id: number) {
    this.router.navigate(['/adminlayout/update-category/', id]);
  }

  onDeleteC(id: number) {
    this.categoryService.deleteCategory(id).subscribe(
      data => {
        console.log(data);
        if (this.isSuccess) {
          return;
        }
        this.isSuccess = true;
        setTimeout(() => this.isSuccess = false, 2500)
        this.getAllCategory();
      },
      error => {
        if (this.isError) {
          return;
        }
        this.isError = true;
        setTimeout(() => this.isError = false, 2500)
      }
    );
  }

}
