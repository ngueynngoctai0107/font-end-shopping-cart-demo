import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Category } from '../../../models/category';
import { AuthService } from '../../../services/auth.service';
import { CategoryService } from '../../../services/category.service';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  form: FormGroup;
  categories: Category[] = [];
  category_id: any;
  imageSrc: string = '';

  public isError: boolean = false;
  public isSuccess: boolean = false;

  constructor(
    private authService: AuthService,
    private categoryService: CategoryService,
    public fb: FormBuilder,
    public productService: ProductService,
    public router: Router
  ) {
    this.form = this.fb.group({
      name: [''],
      price: [''],
      totals: [''],
      image: [null]
    })
  }

  ngOnInit(): void {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    if (!this.authService.getRole()) {
      this.router.navigateByUrl('login');
    }
    this.categoryService.getAll().subscribe(
      data => {
        this.categories = data;
        console.log(data);
      }, error => console.log(error));
  }

  onChangeCategory(event: Event) {
    // console.log(event);
    this.category_id = (event.target as HTMLInputElement).value
	}

  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({
      image: file
    });
    this.form.get('image').updateValueAndValidity()

    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
      };
    }
  }

  submit() {
    this.productService.addProduct(
      this.form.value.name,
      this.form.value.price,
      this.form.value.totals,
      this.category_id,
      this.form.value.image,
    ).subscribe(
      data => {
        console.log(data);
        if (this.isSuccess) {
          return;
        }
        this.isSuccess = true;
        setTimeout(() => this.isSuccess = false, 2500)
      }, (err) => {
        if (this.isError) {
          return;
        }
        this.isError = true;
        setTimeout(() => this.isError = false, 2500)
      });
  }
}

