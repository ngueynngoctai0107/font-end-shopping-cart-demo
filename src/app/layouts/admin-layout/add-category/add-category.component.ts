import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from '../../../models/category';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { CategoryService } from '../../../services/category.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {
  form: FormGroup;

  public isError: boolean = false;
  public isSuccess: boolean = false;

  constructor(private categoryService: CategoryService,
    private router: Router,
    public fb: FormBuilder,
    private authService: AuthService) {
    this.form = this.fb.group({
      name: [''],
      title: [''],
    })
  }

  ngOnInit(): void {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    if (!this.authService.getRole()) {
      this.router.navigateByUrl('login');
    }
  }

  submit() {
    const category: Category = { name: this.form.value.name, title: this.form.value.title };
    this.categoryService.save(category).subscribe(data => {
      console.log(data)
      if (this.isSuccess) {
        return;
      }
      this.isSuccess = true;
      setTimeout(() => this.isSuccess = false, 2500)
    }, (err) => {
      if (this.isError) {
        return;
      }
      this.isError = true;
      setTimeout(() => this.isError = false, 2500)
    });
  }
}
