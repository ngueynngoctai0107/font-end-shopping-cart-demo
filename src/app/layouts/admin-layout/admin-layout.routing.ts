import { Routes } from '@angular/router';
import { ListProductComponent } from './list-product/list-product.component';
import { HomeComponent } from './home/home.component';
import { ListCategoryComponent } from './list-category/list-category.component';
import { AddProductComponent } from './add-product/add-product.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { ProfileComponent } from './profile/profile.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';

export const AdminLayoutRoutes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'list-products',
        component: ListProductComponent
    },
    {
        path: 'list-categories',
        component: ListCategoryComponent
    },
    {
        path: 'add-product',
        component: AddProductComponent
    },
    {
        path: 'add-category',
        component: AddCategoryComponent
    },
    {
        path: 'profile',
        component: ProfileComponent
    },
    {
        path: 'update-product/:id',
        component: EditProductComponent
    }
    ,
    {
        path: 'update-category/:id',
        component: EditCategoryComponent
    }

];
