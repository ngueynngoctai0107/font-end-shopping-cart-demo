import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthService } from '../../../services/auth.service';
import { UserService } from '../../../services/user.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  form: FormGroup;
  avatarUpdate: any;
  usernameget: string;
  user: User;
  imageSrc: string = '';
  base64Data: any;

  oldPass: string;
  newPass: string;

  public isError: boolean = false;
  public isSuccess: boolean = false;

  constructor(private userService: UserService,
    private authService: AuthService,
    public fb: FormBuilder,
    private router: Router) {
    this.form = this.fb.group({
      fullName: [''],
    })
  }

  ngOnInit() {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    if (!this.authService.getRole()) {
      this.router.navigateByUrl('login');
    }
    this.usernameget = this.authService.getSignedinUser();
    this.getUser();
  }

  getUser() {
    this.userService.findUser(this.usernameget).subscribe(
      data => {
        this.user = data;
        console.log("user" + data);
      }, error => console.log(error));
  }

  submit() {
    this.userService.updateUser(
      this.user.id,
      this.form.value.fullName
    ).subscribe(
      data => {
        this.getUser();
        console.log(data)
        if (this.isSuccess) {
          return;
        }
        this.isSuccess = true;
        setTimeout(() => this.isSuccess = false, 2500)
      },
      error => {
        if (this.isError) {
          return;
        }
        this.isError = true;
        setTimeout(() => this.isError = false, 2500)
      }
    );
  }

  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.avatarUpdate = file;
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
      };

    }
  }

  handleImage(picByte) {
    this.base64Data = picByte;
    return 'data:image/jpeg;base64,' + this.base64Data;
  }

  updateAvatar() {
    if (this.avatarUpdate != null) {
      this.userService.updateAvatar(this.user.id, this.avatarUpdate).subscribe(
        data => {
          this.getUser();
          console.log(data)
          if (this.isSuccess) {
            return;
          }
          this.isSuccess = true;
          setTimeout(() => this.isSuccess = false, 2500)
        },
        error => {
          if (this.isError) {
            return;
          }
          this.isError = true;
          setTimeout(() => this.isError = false, 2500)
        }
      );
    }
  }

  updatePassword() {
    this.userService.updatePassword(this.user.id, this.oldPass, this.newPass).subscribe(
      data => {
        if (data.body.message === 'Update password success') {
          this.getUser();
          if (this.isSuccess) {
            return;
          }
          this.isSuccess = true;
          setTimeout(() => this.isSuccess = false, 2500)
        }
        else {
          if (this.isError) {
            return;
          }
          this.isError = true;
          setTimeout(() => this.isError = false, 2500)
        }
      },
      error => {
        if (this.isError) {
          return;
        }
        this.isError = true;
        setTimeout(() => this.isError = false, 2500)
      }
    );
  }

}
