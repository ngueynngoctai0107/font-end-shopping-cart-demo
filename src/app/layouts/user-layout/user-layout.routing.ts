import { Routes } from '@angular/router';
import { HomeComponent } from '../user-layout/home-user/home.component';
import { AddCartComponent } from './add-cart/add-cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ProductComponent } from './product/product.component';
import { CartHistoryComponent } from './cart-history/cart-history.component';

export const UserLayoutRoutes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'add-cart',
        component: AddCartComponent 
    },
    {
        path: 'checkout/:total',
        component: CheckoutComponent 
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'products',
        component: ProductComponent
    },
    {
        path: 'cart-history',
        component: CartHistoryComponent
    },
];
