import { Component, OnInit } from '@angular/core';
import { OrderDetails } from 'src/app/models/order-details';
import { OrderSeive } from 'src/app/services/order.service';

@Component({
  selector: 'app-cart-history',
  templateUrl: './cart-history.component.html',
  styleUrls: ['./cart-history.component.css']
})
export class CartHistoryComponent implements OnInit {
  orderDetails: OrderDetails[];

  constructor(private orderService: OrderSeive) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.orderService.getAll().subscribe(
      data => {
        this.orderDetails = data;
        console.log(data);
      }
    )
  }

  getStatus(value){
    if(value === true) {
      return 'Da thanh toan';
    }
    if(value === false) {
      return 'Chua thanh toan';
    }
    return null;
  }

}
