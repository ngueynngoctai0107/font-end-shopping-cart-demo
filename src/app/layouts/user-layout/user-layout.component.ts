import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-user-layout',
  templateUrl: './user-layout.component.html',
  styleUrls: ['./user-layout.component.css'],
})
export class UserLayoutComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    // if (!this.authService.isUserLoggedIn()) {
    //   this.router.navigateByUrl('login');
    // }
  }

  logOut() {
    this.authService.logout();
  }

  home(){
    this.router.navigateByUrl('userlayout');
  }

}
