import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserLayoutRoutes } from './user-layout.routing';
import { AddCartComponent } from './add-cart/add-cart.component';
import { HomeComponent } from './home-user/home.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ProductComponent } from './product/product.component';
import { CartHistoryComponent } from './cart-history/cart-history.component';
import { CheckoutComponent } from './checkout/checkout.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserLayoutRoutes),
    CarouselModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AddCartComponent,
    HomeComponent,
    ProductComponent,
    CartHistoryComponent,
    CheckoutComponent,
  ]
})

export class UserlayoutModule {}
