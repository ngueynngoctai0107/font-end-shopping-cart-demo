import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Product } from '../../../models/product';
import { AuthService } from '../../../services/auth.service';
import { ProductService } from '../../../services/product.service';
import { CartService } from '../../../services/cart.service';
import { Cart } from 'src/app/models/cart';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products: Product[];
  base64Data: any;
  cartlist: Cart[];
  cart: Cart;
  userId: number;

  public isError: boolean = false;
  public isSuccess: boolean = false;

  constructor(private router: Router, private authService: AuthService, 
      private productService: ProductService, private cartService: CartService,
      private userService: UserService,) { }

  ngOnInit(): void {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    const username = this.authService.getSignedinUser();
    this.userService.findUser(username).subscribe(
      data => this.userId = data.id
    )
    this.getAllProduct();
  }

  getAllProduct() {
    this.productService.getAllProduct().subscribe(
      data => {
        this.products = data;
      },
      error => console.log("khong co quyen"),
    );
  }

  handleImage(picByte) {
    this.base64Data = picByte;
    return 'data:image/jpeg;base64,' + this.base64Data;
  }

  addCart(product_id: number){
    let total = 0;
    this.cartService.getAll().subscribe(data => {
      this.cartlist = data;
      for (let cart of this.cartlist) {
        if (cart.product.id == product_id && cart.userId == this.userId && !cart.checkout) {
          total = cart.quantity + 1;
          this.cart = new Cart(cart.id, this.userId, total, product_id);
          this.cartService.update(this.cart).subscribe(data => {
            if (this.isSuccess) {
              return;
            }
            this.isSuccess = true;
            setTimeout(() => this.isSuccess = false, 2500)
          }, (err) => {
            if (this.isError) {
              return;
            }
            this.isError = true;
            setTimeout(() => this.isError = false, 2500)
          });
          break;
        }
      }
      if (total === 0) {
        this.cart = new Cart(null, this.userId, 1, product_id);
        this.cartService.save(this.cart).subscribe(data => {
          if (this.isSuccess) {
            return;
          }
          this.isSuccess = true;
          setTimeout(() => this.isSuccess = false, 2500)
        }, (err) => {
          if (this.isError) {
            return;
          }
          this.isError = true;
          setTimeout(() => this.isError = false, 2500)
        });
      }
    });
  }


  customOptions: OwlOptions = {
    responsive: {
      0: {
        items: 1 
      },
      2000: {
        items: 2
      },
      4000: {
        items: 3
      }
    },
    nav: false
  }

  customOptions2: OwlOptions = {
    navSpeed: 600,
    margin: 50,
    responsive: {
      0: {
        items: 1 
      },
      1000: {
        items: 2
      },
      2000: {
        items: 3
      },
      3000: {
        items: 4
      }
    },
    nav: false
  }

}
