import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from '../../../services/auth.service';
import { OrderSeive } from '../../../services/order.service';
import { render } from 'creditcardpayments/creditCardPayments'

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  userId: number;
  form: FormGroup;
  totals: number = 0;
  totalAll: number = 0;
  shipping: number = 30;
  isCheckout: boolean = false;
  checkPayment: string = "cash";

  constructor(private router: Router, private orderService: OrderSeive, private route: ActivatedRoute,
    private userService: UserService, private authService: AuthService, public fb: FormBuilder) {
    this.form = this.fb.group({
      name: [''],
      phone: [''],
      address: [''],
    })
  }

  ngOnInit(): void {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    const username = this.authService.getSignedinUser();
    this.userService.findUser(username).subscribe(
      data => this.userId = data.id
    )
    this.totals = this.route.snapshot.params['total'];
    this.totalAll = this.shipping + this.totals;
  }

  changeGender(event: any) {
    this.checkPayment = event.target.value;
  }

  checkout(){
    if(this.checkPayment == "paypal"){
      this.paypal();
    }
    if(this.checkPayment == "cash"){
      this.setCheckout();
    }
  }

  setCheckout() {
    this.orderService.checkout(this.userId, this.form.value.name,
      this.form.value.phone, this.form.value.address, this.totalAll, this.isCheckout).subscribe(
        data => {
          alert('Checkout success');
          this.router.navigateByUrl('userlayout');
        }
    );
  }

  paypal() {
    render(
      {
        id: "#myPaypalButton",
        currency: 'USD',
        value: `${this.totalAll}`,
        onApprove: (details) => {
          this.isCheckout = true;
          this.setCheckout();
        }
      }
    );
  }

}
