import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cart } from '../../../models/cart';
import { AuthService } from '../../../services/auth.service';
import { CartService } from '../../../services/cart.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-add-cart',
  templateUrl: './add-cart.component.html',
  styleUrls: ['./add-cart.component.css']
})
export class AddCartComponent implements OnInit {
  carts: Cart[] = [];
  base64Data: any;
  total = [];
  totalAll: number = 0;
  userId: number;
  checkCartItem: boolean = false;

  constructor(private cartService: CartService, private router: Router,
    private userService: UserService, private authService: AuthService,) { }

  ngOnInit(): void {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('login');
    }
    const username = this.authService.getSignedinUser();
    this.userService.findUser(username).subscribe(
      data => this.userId = data.id
    )
    this.getAll();
  }

  getAll() {
    this.cartService.getAll().subscribe(data => {
      for (let cart of data) {
        if (this.userId == cart.userId) {
          this.carts.push(cart);
          this.checkCartItem = true;
          this.total.push(cart.quantity * cart.product.price);
          this.totalAll = this.totalAll + (cart.quantity * cart.product.price);
        }
      }
    })
  }

  handleImage(picByte) {
    this.base64Data = picByte;
    return 'data:image/jpeg;base64,' + this.base64Data;
  }

  addTotals(id: number, quantity: number) {
    quantity = quantity + 1;
    const cart = new Cart(id, null, quantity, null);
    this.cartService.update(cart).subscribe(data => {
      this.carts = [];
      this.total = [];
      this.totalAll = 0;
      this.getAll()
    });
  }

  minusTotals(id: number, quantity: number) {
    quantity = quantity - 1;
    if (quantity === 0) {
      this.cartService.delete(id).subscribe(data => {
        this.carts = [];
        this.total = [];
        this.totalAll = 0;
        this.getAll()
      });
    } else {
      const cart = new Cart(id, null, quantity, null);
      this.cartService.update(cart).subscribe(data => {
        this.carts = [];
        this.total = [];
        this.totalAll = 0;
        this.getAll()
      });
    }
  }

  checkout() {
    this.router.navigate(['/userlayout/checkout/',this.totalAll]);
  }

  backHome(){
    this.router.navigateByUrl('userlayout');
  }

}
