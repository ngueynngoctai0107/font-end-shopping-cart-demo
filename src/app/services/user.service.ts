import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl = "http://localhost:8082/api/user";

  constructor(private http: HttpClient) { }
  
  public findUser(username: string): Observable<User>{
    return this.http.get<User>(`${this.baseUrl}/find/${username}`, { headers: new HttpHeaders({ 'Content-Type': 'application/json' })});
  }

  public editUser(id: number, user: User): Observable<User>{
    return this.http.put<User>(`${this.baseUrl}/${user.id}`, user);
  }

  public updateUser(id: number, fullName: string): Observable<any>{
    var formData: any = new FormData();
    formData.append('id', new Blob([JSON.stringify(id)], {type: 'application/json'}));
    formData.append('fullName', fullName);
    return this.http.put<any>(`${this.baseUrl}/update/${id}`, formData);
  }

  public updateAvatar(id: number, image: File): Observable<any> {
    var formData: any = new FormData();
    formData.append('id', new Blob([JSON.stringify(id)], {type: 'application/json'}));
    formData.append('avatar', image);
    console.log(id,image);
    return this.http
      .put(`${this.baseUrl}/update-avatar/${id}`, formData, {
        reportProgress: true,
        observe: 'events',
      });
  }

  public updatePassword(id: number, oldPass: string, newPass: string): Observable<any> {
    var formData: any = new FormData();
    formData.append('id', new Blob([JSON.stringify(id)], {type: 'application/json'}));
    formData.append('passwordOld', oldPass);
    formData.append('passwordNew', newPass);
    return this.http
      .put<any>(`${this.baseUrl}/update-password/${id}`, formData, {
        reportProgress: true,
        observe: 'events',
      });
  }
}
