import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from '../models/category';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private baseUrl = 'http://localhost:8082/api/category';

  constructor(private router: Router,
    private http: HttpClient) { }

  public save(category: Category): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/add`, category, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' as 'json' }).pipe(map((resp) => {
      return resp;
    }));
  }

  public getAll(): Observable<any>{
    return this.http.get<any>(this.baseUrl, { headers: new HttpHeaders({ 'Content-Type': 'application/json' })});
  }

  public findById(id: number): Observable<Category>{
    return this.http.get<Category>(`${this.baseUrl}/find/${id}`, { headers: new HttpHeaders({ 'Content-Type': 'application/json' })});
  }

  public deleteCategory(id: number): Observable<any>{
    return this.http.delete<any>(`${this.baseUrl}/delete/${id}`);
  }

  public updateCategory(category: Category): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/update`, category, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' as 'json' }).pipe(map((resp) => {
      return resp;
    }));
  }
}
