import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RequestUser } from '../models/requestUser';

@Injectable({
	providedIn: 'root'
})
export class AuthService {
	private baseUrl = 'http://localhost:8082/api/auth/';
	jwtService: JwtHelperService = new JwtHelperService();

	constructor(private router: Router,
		private http: HttpClient) { }

	signin(request: RequestUser): Observable<any> {
		return this.http.post<any>(`${this.baseUrl}signin`, request, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }).pipe(map((resp) => {
			sessionStorage.setItem("user", request.username);
			sessionStorage.setItem("role", resp.roles[0]);
			let tokenStr = "Bearer " + resp.token;
			sessionStorage.setItem("token", tokenStr);
			return resp;
		}));
	}

	signup(request: RequestUser): Observable<any> {
		return this.http.post<any>(`${this.baseUrl}signup`, request, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' as 'json' }).pipe(map((resp) => {
			return resp;
		}));
	}

	logout() {
		sessionStorage.removeItem('user');
		sessionStorage.removeItem('token');
		sessionStorage.removeItem('role');

		this.router.navigateByUrl('login');
	}

	isUserLoggedIn() {
		let sessionStorageToken = sessionStorage.getItem('token')  as string;

		if (sessionStorageToken) {
			var isTokenExpired = this.jwtService.isTokenExpired(sessionStorageToken);
			if (isTokenExpired) {
				return false;
			} else {
				return !(sessionStorageToken === null);
			}

		}
		return false;
	}

	getSignedinUser() {
		return sessionStorage.getItem('user') as string;
	}

	getRole() {
		let role = sessionStorage.getItem('role') as string;
		if (role=='ADMIN') {
			return true;
		}
		return false;
	}

	getToken() {
		let token = sessionStorage.getItem('token') as string;
		return token
	}
}
