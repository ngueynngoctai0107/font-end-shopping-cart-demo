import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Cart } from '../models/cart';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private baseUrl = 'http://localhost:8082/api/cart';

  constructor(private router: Router,
    private http: HttpClient) { }

  public save(cart: Cart): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/add`, cart).pipe(map((resp) => {
      return resp;
    }));
  }

  public update(cart: Cart): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/update`, cart).pipe(map((resp) => {
      return resp;
    }));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/delete/${id}`);
  }

  public getAll(): Observable<Cart[]> {
    return this.http.get<Cart[]>(`${this.baseUrl}`);
  }
}
