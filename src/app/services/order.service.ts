import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OrderDetails } from '../models/order-details';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class OrderSeive {
    private baseUrl = 'http://localhost:8082/api/order';

    constructor(private http: HttpClient) { }

    public checkout(id: number, name: string, phone: number, address: string, total: number, isCheckout: boolean): Observable<any> {
        var formData: any = new FormData();
        formData.append('id', new Blob([JSON.stringify(id)], { type: 'application/json' }));
        formData.append('name', name);
        formData.append('phone', new Blob([JSON.stringify(phone)], { type: 'application/json' }));
        formData.append('address', address);
        formData.append('totals', new Blob([JSON.stringify(total)], { type: 'application/json' }));
        formData.append('isCheckout', new Blob([JSON.stringify(isCheckout)], { type: 'application/json' }));
        return this.http.post<any>(`${this.baseUrl}/checkout`, formData);
    }

    public getAll(): Observable<OrderDetails[]> {
        return this.http.get<OrderDetails[]>(`${this.baseUrl}`);
    }
}
