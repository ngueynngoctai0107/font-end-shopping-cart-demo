import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private baseUrl = "http://localhost:8082/api/product";

  constructor(private http: HttpClient) { }

  public addProduct(name: string, price: number, totals: number, category_id: number, profileImage: File): Observable<any> {
    var formData: any = new FormData();
    formData.append('name', name);
    formData.append('price', new Blob([JSON.stringify(price)], { type: 'application/json' }));
    formData.append('totals', new Blob([JSON.stringify(totals)], { type: 'application/json' }));
    formData.append('category_id', new Blob([JSON.stringify(category_id)], { type: 'application/json' }));
    formData.append('image', profileImage);
    return this.http
      .post<any>(`${this.baseUrl}/insert`, formData, {
        reportProgress: true,
        observe: 'events',
      });
  }

  public getAllProduct(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}`);
  }

  public updateProduct(id: number, name: string, price: number, totals: number, category_id: number, image: File): Observable<any> {
    var formData: any = new FormData();
    formData.append('id', new Blob([JSON.stringify(id)], { type: 'application/json' }));
    formData.append('name', name);
    formData.append('price', new Blob([JSON.stringify(price)], { type: 'application/json' }));
    formData.append('totals', new Blob([JSON.stringify(totals)], { type: 'application/json' }));
    formData.append('category_id', new Blob([JSON.stringify(category_id)], { type: 'application/json' }));
    formData.append('image', image);
    return this.http
      .put<any>(`${this.baseUrl}/update/${id}`, formData, {
        reportProgress: true,
        observe: 'events',
      });
  }

  public updateProductNoImg(id: number, name: string, price: number, totals: number, category_id: number): Observable<any> {
    var formData: any = new FormData();
    formData.append('id', new Blob([JSON.stringify(id)], { type: 'application/json' }));
    formData.append('name', name);
    formData.append('price', new Blob([JSON.stringify(price)], { type: 'application/json' }));
    formData.append('totals', new Blob([JSON.stringify(totals)], { type: 'application/json' }));
    formData.append('category_id', new Blob([JSON.stringify(category_id)], { type: 'application/json' }));
    return this.http
      .put<any>(`${this.baseUrl}/update-no-img/${id}`, formData, {
        reportProgress: true,
        observe: 'events',
      });
  }

  public deleteProduct(id: number): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/delete/${id}`);
  }

  public findProduct(name: String): Observable<any> {
    var formData: any = new FormData();
    formData.append('name', name);
    return this.http.post<any>(`${this.baseUrl}/findName`, formData);
  }

  public getProductById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/find/${id}`);
  }

}
