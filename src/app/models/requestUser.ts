export interface RequestUser {
	username: string;
	email?: string;
	roles?: string;
	password: string;
	fullName?: string;
}
