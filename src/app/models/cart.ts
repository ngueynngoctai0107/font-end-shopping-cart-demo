import { Product } from "./product";

export class Cart {
    id?: number;
    userId: number;
    quantity: number;
    product?: Product;
    checkout?: boolean;
    productId: number;
    constructor(id: number, userId:number, quantity: number, id_product: number) {
        this.id = id;
        this.userId = userId;
        this.quantity = quantity;
        this.productId = id_product;
    }
}
