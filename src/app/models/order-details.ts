import { Cart } from "./cart";

export class OrderDetails{
    id?: number;
    name?: string;
    phone?: number;
    address?: string;
    totals?: number;
    userId?: number;
    cart?: Cart[];
    checkout?: boolean;
}