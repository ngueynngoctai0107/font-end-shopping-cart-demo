import { Category } from "./category";

export class Product {
    id?: number;
    name?: string;
    price?: number;
    totals?: number;
    category?: Category;
    picByte: any;

    constructor(){
        
    }
    
}
